const express = require("express")
const routes = express.Router()
const db = require("../database/db.js")
const bcrypt = require("bcrypt")
const jwt = require('jsonwebtoken')
const config = require('../config/config.js');
const saltRounds = 10;
const middlewares = require('../middleware/middlewares');


//************************************************************************ */
//PARTIE USER
//************************************************************************ */
//route pour récupérer un utilisateur
routes.get("/users/:id", function (req, res) {
    let id = req.params.id;
    try {
        db.query(`SELECT * FROM users WHERE users.id_user = ${id}`, function (err, result, field) {
            if (result.length == 0) {
                res.status(400).send("Error")
                throw err
            } else {
                res.status(200).send(result)
            }
        })
    }
    catch {
        res.status(200).send(result)
    }
})




//*********************************************************************/
//ROUTE INSCRIPTION UTILISATEUR
//*********************************************************************/

routes.use("/users/sign-up", middlewares.emailMiddleware)

routes.post("/users/sign-up", async function (req, res) {
    const password = req.body.password
    const encryptedPassword = await bcrypt.hash(password, saltRounds) //DÉCLARATION D'UNE VARIABLE QUI CONTIENT LE MOT DE PASSE CRYPTÉ qui sera ensuite enregistré dans la base de données
    try {
        db.query(`INSERT INTO users (last_name, first_name, nickname, adress, phone, email, avatar, password) VALUES ('${req.body.last_name}', '${req.body.first_name}', '${req.body.nickname}', '${req.body.adress}', '${req.body.phone}', '${req.body.email}', '${req.body.avatar}', '${encryptedPassword}')`, function (err, results) {
            console.log(err);
            if (err) {
                res.send({
                    "code": 400,
                    "failed": "error ocurred"
                })
            } else {
                const email = req.body.email;
                db.query(`SELECT * FROM users WHERE email = ?`, [email], async function (error, results, fields) {
                    let token = jwt.sign({ id: results[0].id, last_name: results[0].name }, config.secret, { expiresIn: 86400 })
                    res.send({
                        "code": 200,
                        "sucess": "user registered successfully",
                        auth: true,
                        token: token,
                        user: results[0]
                    })
                })
            }
        })
    } catch (err) {
        res.status(400).send(err)
    }
})

//*********************************************************************/
//ROUTE CONNECTION USER
//*********************************************************************/


routes.post('/users/sign-in', async function (req, res) {
    console.log('req.body')
    console.log(req.body)
    const email = req.body.email //DÉCLARATION D'UNE VARIABLE EMAIL CORRESPONDANT À LA VALEUR SAISIE DANS LE FORMULAIRE
    const password = req.body.password //DÉCLARATION D'UNE VARIABLE MOT DE PASSE CORRESPONDANT À LA VALEUR SAISIE DANS LE FORMULAIRE
    console.log(req.body)
    db.query(`SELECT * FROM users WHERE email = ?`, [email], async function (error, results, fields) { //SÉLECTION DANS LA BASE DE DONNÉES D'UN USER AVEC UNE ADRESSE MAIL CORRESPONDANTE À LA SAISIE
        if (error) { //SI ERREUR ENVOI UN MESSAGE D'ERREUR
            res.send({
                "code": 400,
                "failed": "error ocurred"
            })
        } else {
            if (results.length > 0) { // SI IL EXISTE UN RESULTAT
                const comparision = await bcrypt.compare(password, results[0].password) //DÉCLARATION DE LA VARIABLE COMPARISION QUI COMPARE LE MOT DE PASSE SAISIT ET LE MOT DE PASSE DE LA BASE DE DONNÉES
                if (comparision) { // SI LE COMPARAISON MATCH ALORS ON ENVOIE UN MESSAGE POUR DIRE QUE C'EST BON
                    let token = jwt.sign({ id: results[0].id_user, last_name: results[0].last_name, email: results[0].email }, config.secret, { expiresIn: 86400 }) //INSTALLATION DU TOKEN QUI EXPIRE DANS 24 HEURES
                    res.send({
                        "token": token,
                        "code": 200,
                        "success": "login successfull",
                        auth: true,
                        user: results
                    })
                } else { //SINON ON DIT QUE L'EMAIL ET LE MOT DE PASSE NE CORRESPONDENT PAS
                    res.send({
                        "code": 204,
                        "success": "Email and Password do not match"
                    })
                }
            } else { // OU BIEN QUE L'EMAIL ET LE MOT N'EXISTENT PAS
                res.send({
                    "code": 206,
                    "success": "Email or Password do not exist"
                })
            }
        }
    })
})





//************************************************************************/
//Routes pour poster les entrées, les plats, les desserts et les boissons
//Insérer un plat

// routes.use("/dishes/insertDishes/", middlewares.tokenMiddleware)
routes.post("/dishes/insertDishes/", function (req, res) {
    try {
        db.query(`INSERT INTO dish
            (name_dish, description_dish, price_dish, new_price_dish, allergens_dish,
            weight_dish, nutritional_values_dish, picture_link, category_dish)
            VALUES ('${req.body.name_dish}', '${req.body.description_dish}', '${req.body.price_dish}', 
            '${req.body.new_price_dish}','${req.body.allergens_dish}', 
            '${req.body.nutritional_values_dish}','${req.body.weight_dish}','${req.body.picture_link}', 
            '${req.body.category_dish}')`, function (err, results) {
            if (err) {
                throw err
            } else {
                res.status(200).send("Dish inserted")
            }
        })
    } catch (err) {
        res.status(400).send(err)
    }
})

//récupérer tous les plats

routes.get("/dishes/", function (req, res) {
    try {
        db.query(`SELECT * FROM dish `, [req.params.id], function (err, results) {
            if (err) {
                res.status(400).send("Error")
            } else {
                res.status(200).send(results)
            }
        })
    } catch (err) {
        res.status(400).send("Error")
    }
})

//récupérer un plat
routes.get("/dishes/:id", function (req, res) {
    try {
        db.query(`SELECT * FROM dish WHERE id_dish = ?`, [req.params.id], function (err, results) {
            if (err) {
                res.status(400).send("Error")
            } else {
                res.status(200).send(results)
            }
        })
    } catch (err) {
        res.status(400).send("Error")
    }
})

//route pour récupérer seulement un type de plat
routes.get("/dishes/category/:category", function (req, res) {
    try {
        db.query(`SELECT * FROM dish WHERE category_dish = ?`, [req.params.category], function (err, results) {
            if (err) {
                res.status(400).send("Error")
            } else {
                console.log(results)
                res.status(200).send(results)
            }
        })
    } catch (err) {
        res.status(400).send("Error")
    }
})


routes.get("/menu/", function (req, res) {
    try {
        db.query(`SELECT * FROM dish `, [req.params.id], function (err, results) {
            if (err) {
                res.status(400).send("Error")
            } else {
                res.status(200).send(results)
            }
        })
    } catch (err) {
        res.status(400).send("Error")
    }
})

// routes.use("/commands/", middlewares.tokenMiddleware)
routes.post("/commands/", function (req, res) {
    try {
        console.log("===================================================")
        console.log("contenu de la commande")
        let object = req.body
        console.log(req.body)
        console.log("===================================================")
        let year = new Date().getFullYear()
        let month = new Date().getMonth() + 1
        let day = new Date().getDate()
        let date = year + "-" + month + "-" + day
        let hour = new Date().getHours()
        let minutes = new Date().getMinutes()
        let secondes = new Date().getSeconds()
        let time = hour + ":" + minutes + ":" + secondes
        db.query(`INSERT INTO commands (id_user, commandDate, commandTime) VALUES ('${req.tokenverified.id}', '${date}', '${time}')`, function (err, results) {
            if (err) {
                console.log(err)
                res.status(400).send("Error")
            } else {
                for (let i = 0; i < object.entrees.length; i++) {
                    let objectEntree = object.entrees[i].id_dish;
                    let objectEntreeQuantity = object.entrees[i].quantity;

                    console.log(objectEntree);
                    console.log("La quantité en entrees est : ")
                    console.log(objectEntreeQuantity);
                    console.log("===================================================")
                    db.query(`INSERT INTO item_command (id_dish, id_command, quantity, total) VALUES ('${objectEntree}', '${results.insertId}', '${objectEntreeQuantity}', '0')`)
                };

                for (let i = 0; i < object.plats.length; i++) {
                    let objectPlat = object.plats[i].id_dish;
                    let objectPlatQuantity = object.quantity[i].quantity;
                    console.log(objectPlat);
                    console.log("la quantité en plats est : ")
                    console.log(objectPlatQuantity)
                    db.query(`INSERT INTO item_command (id_dish, id_command, quantity, total) VALUES ('${objectPlat}', '${results.insertId}','1', '0')`)
                };
                for (let i = 0; i < object.desserts.length; i++) {
                    let objectDessert = object.desserts[i].id_dish;
                    console.log(objectDessert);
                    db.query(`INSERT INTO item_command (id_dish, id_command, quantity, total) VALUES ('${objectDessert}', '${results.insertId}','1', '0')`)
                };
                for (let i = 0; i < object.boissons.length; i++) {
                    let objectBoisson = object.boissons[i].id_dish;
                    console.log(objectBoisson);
                    db.query(`INSERT INTO item_command (id_dish, id_command, quantity, total) VALUES ('${objectBoisson}', '${results.insertId}','1', '0')`)
                }
                console.log("===================================================")
                console.log("Mon result =")
                console.log(results)
                res.status(200).send("Command created")
            }
        })
    } catch (err) {
        console.log(err)
        res.status(400).send("Error")
    }
})

routes.post("/cart", function (req, res) {
    try {
        db.query(`INSERT INTO cart (name_dish, description_dish, price_dish, new_price_dish, allergens_dish, weight_dish, nutritional_values_dish, picture_link, category_dish) VALUES ('${req.body.name_dish}', '${req.body.description_dish}', '${req.body.price_dish}', '${req.body.new_price_dish}','${req.body.allergens_dish}', '${req.body.nutritional_values_dish}','${req.body.weight_dish}','${req.body.picture_link}', '${req.body.category_dish}')`, function (err, results) {
            if (err) {
                throw err
            } else {
                res.status(200).send("Dish inserted")
            }
        })
    } catch (err) {
        res.status(400).send(err)
    }
})


module.exports = routes