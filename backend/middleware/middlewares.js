const jwt = require("jsonwebtoken") //pour vérifier les tokens
const config = require("../config/config")
const db = require("../database/db")

//middleware pour vérifier le token
const tokenMiddleware = (req, res, next) => {
    let token = req.headers.authorization
    token = token.split(" ")[1]
    console.log(token)
    let verifiedToken = jwt.verify(token, config.secret)
    console.log(verifiedToken)
    if (verifiedToken) {
        req.tokenverified = verifiedToken
        next()
    } else {
        res.status(403).send("Invalid")
    }
}

//middleware qui permet de vérifier l'email
const emailMiddleware = (req, res, next) => {
    let email = req.body.email
    console.log(req.body);

    db.query(`Select * FROM users WHERE email = ?`, [email], async function (err, results) {
        if (results.length) {
            res.status(400).send("Email already exists")
        } else {
            next()
        }
    })
}

module.exports = { tokenMiddleware, emailMiddleware }