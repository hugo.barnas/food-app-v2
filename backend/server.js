
const express = require("express") //pour la création d'un server 
const cors = require("cors") //cross-Origin Resource Sharing : sécurité qui empéche des requêtes malveillantes
require('dotenv').config()
const api = express() //const permettant la gestion des routes 
const routes = require("./routes/routes.js") // constante permettant d'accéder aux routes qui sont dans un autre dossier

//middleware indispensable pour la gestion des données
api.use(express.urlencoded({ extended: true }));
api.use(express.json());

//middleware qui donne le droit à l'application de consommer accéder l'api
api.use(cors())

//middleware général qui va permettre à l'application d'accéder à l'API sans problème
api.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*") //Tout le monde a le droit d'accéder à notre API -> (*)
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, PATCH, OPTIONS') //Authorisation d'utiliser les méthodes GET, PUT, POST, DELETE
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept") //Authorisation d'utiliser certains en-têtes, certains headers sur l'objet requête
    next(); //permet de passer l'exécution au middleware d'après
});

//Acces aux différentes routes de l'api
api.use("/", routes)

//middleware qui spécifie sur quel port on écoute le serveur
api.listen(8081, () => {
    console.log("Server is running on port 8081");
})