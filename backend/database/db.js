const mysql = require('mysql2')

//connexion à la base de données avec le mot de passe, le nom de la base de données et le port
const connexion = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "Golgot22",
    database: "foodapp",
    port: 3306
})

connexion.connect(
    function (err) {
        if (err) {
            throw err
        }

        try {
            connexion.query("CREATE TABLE IF NOT EXISTS `users`(id_user INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT, last_name VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, nickname VARCHAR(255) NOT NULL, adress VARCHAR(255) NOT NULL, phone INT(10), email VARCHAR(255) NOT NULL, avatar LONGTEXT NOT NULL, password VARCHAR(255) NOT NULL)")
            connexion.query("CREATE TABLE IF NOT EXISTS `commands` (id_command int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT, id_user int(11), commandDate DATE, commandTime TIME, FOREIGN KEY (id_user) REFERENCES users(id_user))")
            //dish
            connexion.query("CREATE TABLE IF NOT EXISTS `dish` (id_dish int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT, name_dish varchar(255) NOT NULL, description_dish LONGTEXT NOT NULL, price_dish int(255) NOT NULL, new_price_dish int(255), allergens_dish LONGTEXT NOT NULL, weight_dish int(255), nutritional_values_dish varchar(255), picture_link LONGTEXT NOT NULL, category_dish LONGTEXT NOT NULL)")
            //iten_command
            connexion.query("CREATE TABLE IF NOT EXISTS `item_command` (id_command_item int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT, id_dish int(11), id_command int(11), quantity INT(255), total INT(255) NOT NULL, FOREIGN KEY (id_dish) REFERENCES dish(id_dish), FOREIGN KEY (id_command) REFERENCES commands(id_command))")
            /**
             * @summary create table for cart
             * id, name_dish, price_dish, new_price_dish, allergns, weight, nutritionnal, picture_link,
             */
            connexion.query("CREATE TABLE IF NOT EXISTS `cart` (id_cart int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT, name_dish varchar(255) NOT NULL, description_dish LONGTEXT NOT NULL, price_dish int(255) NOT NULL, new_price_dish int(255), allergens_dish LONGTEXT NOT NULL, weight_dish int(255), nutritional_values_dish varchar(255), picture_link LONGTEXT NOT NULL, category_dish LONGTEXT NOT NULL, cart_quantity int(11))")
        } catch (err) {
            throw err
        }


        console.log("Connexion established")
    }
)

module.exports = connexion