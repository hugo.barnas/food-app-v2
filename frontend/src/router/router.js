/* eslint-disable */
import Vue from "vue"
import VueRouter from "vue-router"
import home from "../views/home.vue"
import admin from "../views/admin/admin.vue"
import dishes from "../views/admin/dishes.vue"
import account from "../views/account.vue"
import signIn from "../views/signIn.vue"
import signUp from "../views/signUp.vue"
import dishAdd from "../views/admin/dishAdd.vue"
import menu from "../views/menu.vue"
import entree from "../views/entree.vue"
import plat from "../views/plat.vue"
import dessert from "../views/dessert.vue"
import boisson from "../views/boisson.vue"
import dishesList from "../components/admin/dishesList.vue"
import cart from "../views/cart.vue"
import notfound from "../views/notfound.vue"


Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: home
  },
  {
    path: "/signIn",
    name: "signIn",
    component: signIn,
  },
  {
    path: "/signUp",
    name: "signUp",
    component: signUp
  },
  {
    path: "/account",
    name: "account",
    component: account
  },
  {
    path: "/admin",
    name: "admin",
    component: admin
  },
  {
    path: "/admin/dish-add",
    name: "admin-dish-add",
    component: dishAdd
  },
  {
    path: "/menu",
    name: "menu",
    component: menu
  },
  {
    path: "/entree",
    name: "entree",
    component: entree
  },
  {
    path: "/plat",
    name: "plat",
    component: plat
  },
  {
    path: "/dessert",
    name: "dessert",
    component: dessert
  },
  {
    path: "/boisson",
    name: "boisson",
    component: boisson
  },
  {
    path: "/dishes",
    name: "dishes",
    component: dishes
  },
  {
    path: "/admin/dishesList",
    name: "dishesList",
    component: dishesList
  },
  {
    path: "/cart",
    name: "cart",
    component: cart
  },
  {
    path: '/404',
    component: notfound
  },
  {
    path: '*',
    redirect: '/404'
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
