import Vue from "vue";
import Vuex from "vuex";
import dishesServices from "../services/dishesServices";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: null,
    last_name: null,
    id: null,
    dishes: [],
    errors: [],
    success: [],
    entree: [],
    boisson: [],
    dessert: [],
    plat: [],
    cart: {
      entrees: [],
      boissons: [],
      plats: [],
      desserts: []
    },
    command: [],
    cartNumber: 0
  },
  mutations: {
    TOKEN_MUTATION(state, tokenSent) {
      state.token = tokenSent
    },
    NAME_USER(state, userName) {
      state.last_name = userName
    },
    ID_USER(state, id) {
      state.id = id
    },
    DELETE_TOKEN(state) {
      state.token = null;
      state.last_name = null;
      state.id = null;
    },

    GET_DISHES(state, dishes) {
      state.dishes = dishes; //mise à jour de ce morceaux d'état avec les plats qui ont été reçus
    },
    POST_DISH(state, dish) {
      state.dishes = [dish, ...state.dishes];
    },
    GET_DISHES_ERROR(state, error) {
      state.errors = [error, ...state.errors];
    },
    GET_ENTREE_ERROR(state, error) {
      state.errors = [error, ...state.errors];
    },
    GET_BOISSON_ERROR(state, error) {
      state.errors = [error, ...state.errors];
    },
    GET_ENTREE(state, entree) {
      state.entree = entree;
    },
    GET_BOISSON(state, boisson) {
      state.boisson = boisson;
    },
    GET_DESSERT(state, dessert) {
      state.dessert = dessert;
    },
    GET_DESSERT_ERROR(state, error) {
      state.errors = [error, ...state.errors];
    },
    GET_PLAT(state, plat) {
      state.plat = plat;
    },
    GET_PLAT_ERROR(state, error) {
      state.errors = [error, ...state.errors]
    },
    ADD_TO_CART_ENTREE(state, entree) {
      state.cart = [entree, ...state.cart]
    },
    ADD_TO_CART_PLAT(state, plat) {
      state.cart = [plat, ...state.cart]
    },
    ADD_TO_CART_DESSERT(state, dessert) {
      state.cart = [dessert, ...state.cart]
    },
    ADD_TO_CART_BOISSON(state, boisson) {
      state.cart = [boisson, ...state.cart]
    },
    ADD_TO_COMMAND(state, cart) {
      state.command = [cart, ...state.cart]
    },
    UP_DATE_CART_ENTREE(state, cart) {
      state.cartNumber += 1
      state.cart.entrees = cart.entrees
      console.log(cart)
    },
    UP_DATE_CART_PLAT(state, cart) {
      state.cartNumber += 1
      state.cart.plats = cart.plats
      console.log(cart)
    },
    UP_DATE_CART_DESSERT(state, cart) {
      state.cartNumber += 1
      state.cart.desserts = cart.desserts
      console.log(cart)
    },
    UP_DATE_CART_BOISSON(state, cart) {
      state.cartNumber += 1
      state.cart.boissons = cart.boissons
      console.log(cart)
    },
    GET_TOKEN(state, token) {
      state.token = token
    },
    DELETE_ENTREE_FROM_CART(state, entree) {
      state.cartNumber -= 1
      state.cart.entrees = entree.entrees
    },
    DELETE_PLAT_FROM_CART(state, plat) {
      state.cartNumber -= 1
      state.cart.plats = plat.plats
    },
    DELETE_DESSERT_FROM_CART(state, dessert) {
      state.cartNumber -= 1
      state.cart.desserts = dessert.desserts
    },
    DELETE_BOISSON_FROM_CART(state, boisson) {
      state.cartNumber -= 1
      state.cart.boissons = boisson.boissons
    }
  },
  actions: {
    sentToken(context, tokenSent) {
      context.commit('TOKEN_MUTATION', tokenSent)
    },
    deleteToken(store) {
      store.commit('DELETE_TOKEN')
    },
    nameUser(context, userName) {
      console.log("je suis dans l'action : ", userName)
      context.commit('NAME_USER', userName)
    },
    idUser(context, idUser) {
      console.log("je suis dans l'action : ", idUser)
      context.commit('ID_USER', idUser)
    },
    getDishes({ commit }) {
      dishesServices.getDishes()
        .then(res => {
          commit("GET_DISHES", res.data);
        })
        // .catch(err => console.error(err.message));
        .catch(err => {
          const error = {
            date: new Date(),
            message: `failed to retrieve products: ${err.message}`
          }
          commit("GET_DISHES_ERROR", error)
        });
    },
    postDish({ commit }, dish) {
      dishesServices.postDish(dish)
        .then(() => {
          commit("POST_DISH", dish);
        })
        .catch(err => console.error(err.message))
    },
    getToken(context, getToken) {
      context.commit('GET_TOKEN', getToken)
    },

    getEntree({ commit }) {
      dishesServices.getEntree()
        .then(res => {
          commit("GET_ENTREE", res.data)
        })
        .catch(err => {
          const error = {
            date: new Date(),
            message: `failed to retrieve products: ${err.message}`
          }
          commit("GET_ENTREE_ERROR", error)
        });
    },
    getBoisson({ commit }) {
      dishesServices.getBoisson()
        .then(res => {
          commit("GET_BOISSON", res.data)
        })
        .catch(err => {
          const error = {
            date: new Date(),
            message: `failed to retrieve products: ${err.message}`
          }
          commit("GET_BOISSON_ERROR", error)
        });
    },
    getDessert({ commit }) {
      dishesServices.getDessert()
        .then(res => {
          commit("GET_DESSERT", res.data)
        })
        .catch(err => {
          const error = {
            date: new Date(),
            message: `failed to retrieve desserts: ${err.message}`
          }
          commit("GET_DESSERT_ERROR", error)
        })
    },
    getPlat({ commit }) {
      dishesServices.getPlat()
        .then(res => {
          commit("GET_PLAT", res.data)
        })
        .catch(err => {
          const error = {
            date: new Date(),
            message: `failed to retrieve desserts: ${err.message}`
          }
          commit("GET_PLAT_ERROR", error)
        })
    },
    addToCartEntree({ commit }, entree) {
      dishesServices.addToCartEntree(entree)
        .then(() => {
          commit("ADD_TO_CART_ENTREE", entree);
        })
        .catch(err => console.error(err));
    },
    addToCartPlat({ commit }, plat) {
      dishesServices.addToCartPlat(plat)
        .then(() => {
          commit("ADD_TO_CART_PLAT", plat);
        })
        .catch(err => console.error(err));
    },
    addToCartDessert({ commit }, dessert) {
      dishesServices.addToCartDessert(dessert)
        .then(() => {
          commit("ADD_TO_CART_DESSERT", dessert)
        })
        .catch(err => console.error(err));
    },
    addToCartBoisson({ commit }, boisson) {
      dishesServices.addToCartBoisson(boisson)
        .then(() => {
          commit("ADD_TO_CART_BOISSON", boisson)
        })
    },

    addToCommand({ commit }, cart) {
      dishesServices.addToCommand(cart)
        .then(() => {
          commit("ADD_TO_COMMAND", cart)
        })
    },

    updateCartEntree({ commit }, entree) {
      dishesServices
        .updateCartEntree(entree)
        .then(() => {
          commit("UP_DATE_CART_ENTREE", JSON.parse(localStorage.getItem('vuex-cart'))
          ); //on récupère le contenu du caddie
        })
        .catch(err => console.log(err));
    },
    updateCartPlat({ commit }, plat) {
      dishesServices
        .updateCartPlat(plat)
        .then(() => {
          commit("UP_DATE_CART_PLAT", JSON.parse(localStorage.getItem('vuex-cart2'))
          ); //on récupère le contenu du caddie
        })
        .catch(err => console.log(err));
    },
    updateCartDessert({ commit }, dessert) {
      dishesServices
        .updateCartDessert(dessert)
        .then(() => {
          commit("UP_DATE_CART_DESSERT", JSON.parse(localStorage.getItem('vuex-cart3'))
          ); //on récupère le contenu du caddie
        })
        .catch(err => console.log(err));
    },
    updateCartBoisson({ commit }, boisson) {
      dishesServices
        .updateCartBoisson(boisson)
        .then(() => {
          commit("UP_DATE_CART_BOISSON", JSON.parse(localStorage.getItem('vuex-cart4'))
          ); //on récupère le contenu du caddie
        })
        .catch(err => console.log(err));
    },

    deleteEntreeFromCart({ commit }, entree) {
      return dishesServices
        .deleteEntreeFromCart(entree)
        .then(() => {
          commit("DELETE_ENTREE_FROM_CART", JSON.parse(localStorage.getItem('vuex-cart')));
        })
        .catch(err => console.log(err));
    },
    deletePlatFromCart({ commit }, plat) {
      return dishesServices
        .deletePlatFromCart(plat)
        .then(() => {
          commit("DELETE_PLAT_FROM_CART", JSON.parse(localStorage.getItem('vuex-cart2')));
        })
        .catch(err => console.log(err));
    },
    deleteDessertFromCart({ commit }, dessert) {
      return dishesServices
        .deleteDessertFromCart(dessert)
        .then(() => {
          commit("DELETE_DESSERT_FROM_CART", JSON.parse(localStorage.getItem('vuex-cart3')));
        })
        .catch(err => console.log(err));
    },
    deleteBoissonFromCart({ commit }, boisson) {
      return dishesServices
        .deleteBoissonFromCart(boisson)
        .then(() => {
          commit("DELETE_BOISSON_FROM_CART", JSON.parse(localStorage.getItem('vuex-cart4')));
        })
        .catch(err => console.log(err));
    },
  },
  getters: {
    getTokenAuthorisation(state){
      return state.token
    },
    getCart(state) {
      return state.cart;
    },
    getEntrees(state) {
      return state.cart.entrees
    },
    getPlats(state) {
      console.log(state.cart.plats)
      return state.cart.plats
    },
    getDesserts(state) {
      console.log(state.cart.desserts)
      return state.cart.desserts
    },
    getBoissons(state) {
      console.log(state.cart.boissons)
      return state.cart.boissons
    },
    getCartNumber(state) {
      return state.cartNumber
    },
    getName(state) {
      return state.last_name
    },
    getNumberCartEntrees: (state) => {
      let resultat = 0
      if (!state.cart.entrees) {
        console.log("IFFFFFFF")
      } else {
        resultat = state.cart.entrees.length
      }
      return resultat
    },

    getNumberCartPlats: (state) => {
      let resultat = 0
      if (!state.cart.plats) {
        console.log("IFFFFFFF")
      } else {
        resultat = state.cart.plats.length
      }
      return resultat
    },

    getNumberCart: (state) => {
      console.log("aAVANT   IFFFFFFF")

      let resultat = 0
      if (!state.cart.entrees) {
        console.log("IFFFFFFF")
      } else {
        resultat = state.cart.entrees.length
      }

      if (!state.cart.plats) {
        console.log("IFFFFFFF")
      } else {
        resultat += state.cart.plats.length
      }
      // console.log(getters.getNumberCart.length)
      // console.log(getters.getNumberCart.length)
      // return getters.getNumberCart.length
      return resultat
    },
  },


});
