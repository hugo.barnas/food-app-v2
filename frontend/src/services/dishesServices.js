import axios from "axios"

let baseUrldishes = "http://localhost:8081"

export default {
    getDishes() {
        return axios.get(`${baseUrldishes}/dishes`);
    },
    postDish(dish) {
        return axios.post(`${baseUrldishes}/dishes/insertDishes`, dish);
    },
    getEntree() {
        return axios.get(`${baseUrldishes}/dishes/category/entree`);
    },
    getPlat() {
        return axios.get(`${baseUrldishes}/dishes/category/plat`);
    },
    getDessert() {
        return axios.get(`${baseUrldishes}/dishes/category/dessert`);
    },
    getBoisson() {
        return axios.get(`${baseUrldishes}/dishes/category/boisson`);
    },
    addToCartEntree(entree) {
        return axios.post(`${baseUrldishes}/cart`, entree);
    },
    addToCartPlat(plat) {
        return axios.post(`${baseUrldishes}/cart`, plat);
    },
    addToCartDessert(dessert) {
        return axios.post(`${baseUrldishes}/cart`, dessert)
    },
    addToCartBoisson(boisson) {
        return axios.post(`${baseUrldishes}/cart`, boisson)
    },
    updateCartEntree(entree) {
        //return axios.post(`${baseUrldishes}/cart`, cart)
        //utilisation de localstorage
        return new Promise((resolve) => {
            let cartInlocalStorage = localStorage.getItem('vuex-cart');
            console.log(cartInlocalStorage) //on cherche un clé correspondant au caddie récupérée dans une string
            let cart = {}; //on déclare une variable cart dans laquelle on va insérer un tableau d'objet qui contient les plats
            //2 cas : si panier vide alors on ajoute une quantité et la clé qui sera stingifiée dans le caddie
            if (!cartInlocalStorage) {
                entree.quantity = 1; //ajout d'un quantité égal à 1
                cart = { entrees: [entree] }; //ajout d'une commande contenu dans un tableau
                localStorage.setItem('vuex-cart', JSON.stringify(cart)); //On met à jour le contenu du nouveau caddie
                resolve(cart)
                console.log(cart);//on résoud
            } else {
                const entrees = JSON.parse(localStorage.getItem('vuex-cart')).entrees;
                console.log(entrees)//on récupère ce qu'il y a dans le caddie
                // s'il y déjà un plat dans le caddie
                const index = entrees.findIndex(c => c.id_dish === entree.id_dish); //tester si ce plat existe déjà dans le caddie à l'aide de son id
                console.log(index)
                console.log(entrees)
                if (index === -1) { //Si la commande n'est pas dans le caddie
                    entree.quantity = 1;
                    console.log(entree.quantity)
                    cart = { entrees: [entree, ...entrees] }
                    console.log(cart); // on créer le caddie en le mettant à jour 
                } else {
                    console.log("je suis dans le else") //si le plat est dans le caddie
                    entrees[index].quantity += 1; //on récupère ce plat et on modifie la quantité en l'agrémentant de 1
                    cart = {
                        entrees: [...entrees] //on conserve le contenu du caddie
                    }
                    console.log(cart)
                }
            }
            localStorage.setItem('vuex-cart', JSON.stringify(cart));//écraser l'ancien contenu avec le nouveau 
            resolve(cart);
            // localStorage.clear();
            return axios.post(`${baseUrldishes}/cart`, entree)
        });
    },
    updateCartPlat(plat) {
        //return axios.post(`${baseUrldishes}/cart`, cart)
        //utilisation de localstorage
        return new Promise((resolve) => {
            let cartInlocalStorage = localStorage.getItem('vuex-cart2'); //on cherche un clé correspondant au caddie récupérée dans une string
            let cart = {}; //on déclare une variable cart dans laquelle on va insérer un tableau d'objet qui contient les plats
            //2 cas : si panier vide alors on ajoute une quantité et la clé qui sera stingifiée dans le caddie
            if (!cartInlocalStorage) {
                plat.quantity = 1; //ajout d'un quantité égal à 1
                cart = { plats: [plat] }; //ajout d'une commande contenu dans un tableau
                localStorage.setItem('vuex-cart2', JSON.stringify(cart)); //On met à jour le contenu du nouveau caddie
                resolve(cart);//on résoud
            } else {
                const plats = JSON.parse(localStorage.getItem('vuex-cart2')).plats; //on récupère ce qu'il y a dans le caddie
                // s'il y déjà un plat dans le caddie
                const index = plats.findIndex(p => p.id_dish === plat.id_dish); //tester si ce plat existe déjà dans le caddie à l'aide de son id
                if (index === -1) { //la commande n'est pas dans le caddie
                    plat.quantity = 1;
                    cart = { plats: [plat, ...plats] }; // on créer le caddie en le mettant à jour 
                } else { //si le plat est dans le caddie
                    plats[index].quantity += 1; //on récupère ce plat et on modifie la quantité en l'agrémentant de 1
                    cart = {
                        plats: [...plats] //on conserve le contenu du caddie
                    }
                }
            }
            localStorage.setItem('vuex-cart2', JSON.stringify(cart)); //écraser l'ancien contenu avec le nouveau 
            resolve(cart);
            // localStorage.clear();
            return axios.post(`${baseUrldishes}/cart`, plat)
        });
    },
    updateCartDessert(dessert) {
        //return axios.post(`${baseUrldishes}/cart`, cart)
        //utilisation de localstorage
        return new Promise((resolve) => {
            let cartInlocalStorage = localStorage.getItem('vuex-cart3'); //on cherche un clé correspondant au caddie récupérée dans une string
            let cart = {}; //on déclare une variable cart dans laquelle on va insérer un tableau d'objet qui contient les plats
            //2 cas : si panier vide alors on ajoute une quantité et la clé qui sera stingifiée dans le caddie
            if (!cartInlocalStorage) {
                dessert.quantity = 1; //ajout d'un quantité égal à 1
                cart = { desserts: [dessert] }; //ajout d'une commande contenu dans un tableau
                localStorage.setItem('vuex-cart3', JSON.stringify(cart)); //On met à jour le contenu du nouveau caddie
                resolve(cart);//on résoud
            } else {
                const desserts = JSON.parse(localStorage.getItem('vuex-cart3')).desserts; //on récupère ce qu'il y a dans le caddie
                // s'il y déjà un plat dans le caddie
                const index = desserts.findIndex(d => d.id_dish === dessert.id_dish); //tester si ce plat existe déjà dans le caddie à l'aide de son id
                if (index === -1) { //la commande n'est pas dans le caddie
                    dessert.quantity = 1;
                    cart = { plats: [dessert, ...desserts] }; // on créer le caddie en le mettant à jour 
                } else { //si le plat est dans le caddie
                    desserts[index].quantity += 1; //on récupère ce plat et on modifie la quantité en l'agrémentant de 1
                    cart = {
                        desserts: [...desserts] //on conserve le contenu du caddie
                    }
                }
            }
            localStorage.setItem('vuex-cart3', JSON.stringify(cart)); //écraser l'ancien contenu avec le nouveau 
            resolve(cart);
            // localStorage.clear();
            return axios.post(`${baseUrldishes}/cart`, dessert)
        });
    },
    updateCartBoisson(boisson) {
        //return axios.post(`${baseUrldishes}/cart`, cart)
        //utilisation de localstorage
        return new Promise((resolve) => {
            let cartInlocalStorage = localStorage.getItem('vuex-cart4'); //on cherche un clé correspondant au caddie récupérée dans une string
            let cart = {}; //on déclare une variable cart dans laquelle on va insérer un tableau d'objet qui contient les plats
            //2 cas : si panier vide alors on ajoute une quantité et la clé qui sera stingifiée dans le caddie
            if (!cartInlocalStorage) {
                boisson.quantity = 1; //ajout d'un quantité égal à 1
                cart = { boissons: [boisson] }; //ajout d'une commande contenu dans un tableau
                localStorage.setItem('vuex-cart4', JSON.stringify(cart)); //On met à jour le contenu du nouveau caddie
                resolve(cart);//on résoud
            } else {
                const boissons = JSON.parse(localStorage.getItem('vuex-cart4')).boissons; //on récupère ce qu'il y a dans le caddie
                // s'il y déjà un plat dans le caddie
                const index = boissons.findIndex(d => d.id_dish === boisson.id_dish); //tester si ce plat existe déjà dans le caddie à l'aide de son id
                if (index === -1) { //la commande n'est pas dans le caddie
                    boisson.quantity = 1;
                    cart = { boissons: [boisson, ...boissons] }; // on créer le caddie en le mettant à jour 
                } else { //si le plat est dans le caddie
                    boissons[index].quantity += 1; //on récupère ce plat et on modifie la quantité en l'agrémentant de 1
                    cart = {
                        boissons: [...boissons] //on conserve le contenu du caddie
                    }
                }
            }
            localStorage.setItem('vuex-cart4', JSON.stringify(cart)); //écraser l'ancien contenu avec le nouveau 
            resolve(cart);
            // localStorage.clear();
            return axios.post(`${baseUrldishes}/cart`, boisson)
        });
    },
    deleteEntreeFromCart(entree) {
        return new Promise(resolve => {
            const entrees = JSON.parse(localStorage.getItem("vuex-cart")).entrees; //récupération de l'objet entrees
            console.log(entrees)
            console.log(entree)
            const index = entrees.findIndex(e => e.id_dish === entree.id_dish); //trouver l'index de l'entree à supprimer
            console.log(index)
            console.log(entree)
            entrees[index].quantity -= 1; //on décrémente la quantité de cette entrée
            //supprimer cette entrée du panier quand la nouvelle quantité est égale à 0
            if (entrees[index].quantity === 0) {
                entrees.splice(index, 1);
            }

            const cart = {
                entrees: entrees
            };
            localStorage.setItem("vuex-cart", JSON.stringify(cart));
            resolve(cart)
            return axios.post(`${baseUrldishes}/cart`, entree)
        })
    },
    deletePlatFromCart(plat) {
        return new Promise(resolve => {
            const plats = JSON.parse(localStorage.getItem("vuex-cart2")).plats; //récupération de l'objet entrees
            const index = plats.findIndex(p => p.id_dish === plat.id_dish); //trouver l'index de l'entree à supprimer
            plats[index].quantity -= 1; //on décrémente la quantité de cette entrée
            //supprimer cette entrée du panier quand la nouvelle quantité est égale à 0
            if (plats[index].quantity === 0) {
                plats.splice(index, 1);
            }

            const cart = {
                plats: plats
            };
            localStorage.setItem("vuex-cart2", JSON.stringify(cart));
            resolve(cart)
            return axios.post(`${baseUrldishes}/cart`, plat)
        })
    },
    deleteDessertFromCart(dessert) {
        return new Promise(resolve => {
            const desserts = JSON.parse(localStorage.getItem("vuex-cart3")).desserts; //récupération de l'objet entrees
            const index = desserts.findIndex(d => d.id_dish === dessert.id_dish); //trouver l'index de l'entree à supprimer
            desserts[index].quantity -= 1; //on décrémente la quantité de cette entrée
            //supprimer cette entrée du panier quand la nouvelle quantité est égale à 0
            if (desserts[index].quantity === 0) {
                desserts.splice(index, 1);
            }

            const cart = {
                desserts: desserts
            };
            localStorage.setItem("vuex-cart3", JSON.stringify(cart));
            resolve(cart)
            return axios.post(`${baseUrldishes}/cart`, dessert)
        })
    },
    deleteBoissonFromCart(boisson) {
        return new Promise(resolve => {
            const boissons = JSON.parse(localStorage.getItem("vuex-cart4")).boissons; //récupération de l'objet entrees
            const index = boissons.findIndex(d => d.id_dish === boisson.id_dish); //trouver l'index de l'entree à supprimer
            boissons[index].quantity -= 1; //on décrémente la quantité de cette entrée
            //supprimer cette entrée du panier quand la nouvelle quantité est égale à 0
            if (boissons[index].quantity === 0) {
                boissons.splice(index, 1);
            }

            const cart = {
                boissons: boissons
            };
            localStorage.setItem("vuex-cart4", JSON.stringify(cart));
            resolve(cart)
            return axios.post(`${baseUrldishes}/cart`, boisson)
        })
    },

};